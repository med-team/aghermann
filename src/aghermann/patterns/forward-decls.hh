/*
 *       File name:  aghermann/patterns/forward-decls.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-07
 *
 *         Purpose:  
 *
 *         License:  GPL
 */

#ifndef AGH_AGHERMANN_PATTERNS_FORWARD_DECLS_H_
#define AGH_AGHERMANN_PATTERNS_FORWARD_DECLS_H_

namespace agh {
namespace pattern {

template <typename T> class CMatch;
template <typename T> struct SPatternPPack;
template <typename T> class CPatternTool;
template <typename T> struct SPattern;

}
}

#endif
