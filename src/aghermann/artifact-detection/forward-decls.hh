/*
 *       File name:  aghermann/artifact-detection/forward-decls.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-10-16
 *
 *         Purpose:  forward declarations of ::agh::ad classes
 *
 *         License:  GPL
 */


#ifndef AGH_AGHERMANN_ARTIFACT_DETECTION_FORWARD_DECLS_H_
#define AGH_AGHERMANN_ARTIFACT_DETECTION_FORWARD_DECLS_H_

namespace agh {
namespace ad {

struct SComprehensiveArtifactDetectionPPack;
class CComprehensiveArtifactDetector;

}} // namespace agh::ad

#endif
