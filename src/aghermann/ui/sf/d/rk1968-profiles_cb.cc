/*
 *       File name:  aghermann/ui/sf/d/rk1968-profiles_cb.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-15
 *
 *         Purpose:  scoring facility: RK1968 dialog callbacks
 *
 *         License:  GPL
 */

#include "rk1968.hh"

using namespace std;
using namespace agh::ui;

extern "C" {

void eSFRKProfileList_changed_cb(
        GtkComboBox*,
        gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;

        RK.eXProfileList_changed_cb();
}




void
bSFRKProfileSave_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;

        RK.bXProfileSave_clicked_cb();
}


void
bSFRKProfileDiscard_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;

        RK.bXProfileDiscard_clicked_cb();
}


void
bSFRKProfileRevert_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;

        RK.bXProfileRevert_clicked_cb();
}



void eSFRKProfileSaveName_changed_cb(
        GtkEditable*,
        const gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;

        RK.eXProfileSaveName_changed_cb();
}

void
eSFRK_any_profile_origin_toggled_cb(
        GtkRadioButton*,
        gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;

        RK.eX_any_profile_origin_toggled_cb();
}



} // extern "C"
