/*
 *       File name:  aghermann/ui/sf/d/filters-construct.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-10-24
 *
 *         Purpose:  scoring facility Filters construct
 *
 *         License:  GPL
 */

#include <stdexcept>

#include "aghermann/ui/ui.hh"
#include "aghermann/ui/mw/mw.hh"
#include "filters.hh"

using namespace std;
using namespace agh::ui;


SFiltersDialogWidgets::
SFiltersDialogWidgets ()
{
        builder = gtk_builder_new();
        if ( !gtk_builder_add_from_resource( builder, "/org/gtk/aghermann/sf-filters.glade", NULL) )
                throw runtime_error( "Failed to load SF::artifacts glade resource");
        gtk_builder_connect_signals( builder, NULL);

        AGH_GBGETOBJ (wSFFilters);
        AGH_GBGETOBJ (lSFFilterCaption);
        AGH_GBGETOBJ (eSFFilterLowPassCutoff);
        AGH_GBGETOBJ (eSFFilterLowPassOrder);
        AGH_GBGETOBJ (eSFFilterHighPassCutoff);
        AGH_GBGETOBJ (eSFFilterHighPassOrder);
        AGH_GBGETOBJ (eSFFilterNotchFilter);
        AGH_GBGETOBJ (mSFFilterNotchFilter);
        AGH_GBGETOBJ (bSFFilterOK);

        gtk_combo_box_set_model_properly(
                eSFFilterNotchFilter, mSFFilterNotchFilter); // can't reuse _p.mNotchFilter

        G_CONNECT_2 (eSFFilterHighPassCutoff, value, changed);
        G_CONNECT_2 (eSFFilterLowPassCutoff, value, changed);
}

SFiltersDialogWidgets::
~SFiltersDialogWidgets ()
{
        // destroy toplevels
        gtk_widget_destroy( (GtkWidget*)wSFFilters);
        g_object_unref( (GObject*)builder);
}
