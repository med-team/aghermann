/*
 *       File name:  aghermann/ui/sf/d/artifacts-profiles_cb.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-21
 *
 *         Purpose:  scoring facility: artifacts dialog profile mgmt callbacks
 *
 *         License:  GPL
 */

#include "artifacts.hh"

using namespace std;
using namespace agh::ui;

extern "C" {

void eSFADProfileList_changed_cb(
        GtkComboBox*,
        gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        for ( auto& W : {AD.cSFADMCBasedExpander, AD.cSFADFlatExpander, AD.cSFADEMGExpander} ) {
                g_signal_emit_by_name( W, "activate");
                g_signal_emit_by_name( W, "activate");
        }

        AD.eXProfileList_changed_cb();
}




void
bSFADProfileSave_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        AD.bXProfileSave_clicked_cb();
}


void
bSFADProfileDiscard_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        AD.bXProfileDiscard_clicked_cb();
}


void
bSFADProfileRevert_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        AD.bXProfileRevert_clicked_cb();
}



void eSFADProfileSaveName_changed_cb(
        GtkEditable*,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        AD.eXProfileSaveName_changed_cb();
}

void
eSFAD_any_profile_origin_toggled_cb(
        GtkRadioButton*,
        const gpointer userdata)
{
        auto& AD = *(SScoringFacility::SArtifactsDialog*)userdata;

        AD.eX_any_profile_origin_toggled_cb();
}



} // extern "C"
