/*
 *       File name:  aghermann/ui/sf/d/rk1968_cb.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-07
 *
 *         Purpose:  scoring facility: RK1968 dialog callbacks
 *
 *         License:  GPL
 */

#include "rk1968.hh"
#include "aghermann/globals.hh"

using namespace std;
using namespace agh::ui;

extern "C" {

void
wSFRK_show_cb(
        GtkWidget*,
        const gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;
        auto& SF = RK._p;
        SF.rk1968_dialog_shown = true;

      // 1. profiles
        RK.load_profiles();
        APPLOG_INFO ("loaded %zu scripts", RK.profiles.size());

        if ( RK.profiles.empty() )
                RK.profiles.emplace_back(
                        *SF._p.ED,
                        agh::SExpDirLevelId {SF._p.ED->group_of(SF.csubject()), SF.csubject().id, SF.session()});

        RK.populate_combo();
        if ( gtk_combo_box_get_active( RK.eSFRKProfileList) == -1 )
                gtk_combo_box_set_active( RK.eSFRKProfileList, 0);
        RK.set_profile_manage_buttons_visibility();

      // 2. service backup
        RK.backup.resize(SF.sepisode().sources.front().n_pages());
        RK.backup = SF.sepisode().sources.front().pages();

      // 3. info
        size_t n_available_channels = 0;
        for ( auto &R : SF.sepisode().recordings )
                if ( R.second.psd_profile.have_data() )
                        ++n_available_channels;
        gtk_label_set_markup(
                RK.lSFRKTopInfo,
                snprintf_buf(
                        "<b>RK1968</b> (%zu EEG channel%s)",
                        n_available_channels, (n_available_channels == 1) ? "" : "s"));
        gtk_label_set_markup(
                RK.lSFRKBottomInfo,
                "");

      // 4. set up try/apply buttons
        RK.suppress_preview_handler = true;
        gtk_toggle_button_set_active( RK.bSFRKPreview, FALSE);
        RK.suppress_preview_handler = false;

        gtk_widget_set_sensitive( (GtkWidget*)RK.bSFRKPreview, TRUE);
        gtk_widget_set_sensitive( (GtkWidget*)RK.bSFRKApply, FALSE);
}

void
wSFRK_hide_cb(
        GtkWidget*,
        gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;
        auto& SF = RK._p;
        SF.rk1968_dialog_shown = false;

        RK.save_profiles();
}

void
eSFRK_any_profile_value_changed_cb(
        GtkSpinButton* button,
        const gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;

        RK.W_V.down();
        if ( RK.Pp2.compile() ) {
                gtk_widget_set_sensitive( (GtkWidget*)RK.bSFRKPreview, FALSE);
                gtk_label_set_markup(
                        RK.lSFRKBottomInfo, RK.Pp2.specific_error.c_str());
        } else {
                gtk_widget_set_sensitive( (GtkWidget*)RK.bSFRKPreview, TRUE);
                gtk_label_set_markup(
                        RK.lSFRKBottomInfo, "");
        }

        RK.eX_any_profile_value_changed_cb();
}


void
bSFRKPreview_toggled_cb(
        GtkToggleButton* button,
        gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;
        auto& SF = RK._p;
        if ( RK.suppress_preview_handler )
                return;

        if ( gtk_toggle_button_get_active(button) ) {
                SBusyBlock bb (RK.wSFRK);

                RK.W_V.down();
                int pages_scored_successfuly;
                auto res = RK.Pp2.score( SF.sepisode(), &pages_scored_successfuly);
                if ( res == agh::rk1968::CScoreAssistant::TScoreErrors::ok ) {
                        SF.get_hypnogram();
                        gtk_widget_set_sensitive(
                                (GtkWidget*)RK.bSFRKApply,
                                TRUE);
                        gtk_label_set_markup(
                                RK.lSFRKBottomInfo,
                                snprintf_buf(
                                        "<small>Scored %d page%s</small>",
                                        pages_scored_successfuly, (pages_scored_successfuly == 1) ? "" : "s"));
                } else {
                        pop_ok_message(
                                (GtkWindow*)RK.wSFRK,
                                "Scoring script problem",
                                "%s", agh::rk1968::CScoreAssistant::score_error_s(res));
                        RK.suppress_preview_handler = true;
                        gtk_toggle_button_set_active( button, FALSE);
                        RK.suppress_preview_handler = false;
                        return;
                }

        } else {
                SF.sepisode().sources.front().pages() = RK.backup;

                SF.get_hypnogram();

                gtk_widget_set_sensitive( (GtkWidget*)RK.bSFRKApply, FALSE);
        }

        SF.calculate_scored_percent(), SF.draw_score_stats();
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
        gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);
}

void
bSFRKApply_clicked_cb(
        GtkButton*,
        gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;

        gtk_widget_hide( (GtkWidget*)RK.wSFRK);
}

void
bSFRKDismiss_clicked_cb(
        GtkButton*,
        const gpointer userdata)
{
        auto& RK = *(SScoringFacility::SRK1968Dialog*)userdata;
        auto& SF = RK._p;

        gtk_widget_hide( (GtkWidget*)RK.wSFRK);

        if ( TRUE == gtk_toggle_button_get_active(RK.bSFRKPreview) ) {
                SF.sepisode().sources.front().pages() = RK.backup;

                SF.get_hypnogram();

                gtk_widget_queue_draw( (GtkWidget*)SF.daSFMontage);
                gtk_widget_queue_draw( (GtkWidget*)SF.daSFHypnogram);
        }
}


} // extern "C"
