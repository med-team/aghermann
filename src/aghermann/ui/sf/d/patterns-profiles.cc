/*
 *       File name:  aghermann/ui/sf/d/patterns-profiles.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-01-16
 *
 *         Purpose:  scoring facility patterns (enumerating & io)
 *
 *         License:  GPL
 */

#include <tuple>

#include "aghermann/ui/misc.hh"
#include "aghermann/ui/mw/mw.hh"
#include "aghermann/ui/sf/channel.hh"
#include "patterns.hh"

using namespace std;
using namespace agh::ui;


int
SScoringFacility::SPatternsDialog::
import_from_selection( SScoringFacility::SChannel& field)
{
        // double check, possibly redundant after due check in callback
        double  run_time = field.selection_end_time - field.selection_start_time;
        size_t  run = field.selection_end - field.selection_start;
        if ( run == 0 )
                return -1;
        if ( run_time > 60. ) {
                pop_ok_message(
                        (GtkWindow*)wSFFD,
                        "<b>Selection greater than a minute</b>",
                        "This is surely the single occurrence, I tell you!");
                return -2;
        }
        if ( run_time > 10. and
             GTK_RESPONSE_YES !=
             pop_question(
                     (GtkWindow*)wSFFD,
                     "<b>The selection is greater than 10 sec</b>",
                     "Sure to proceed with search?") )
                return -3;

        gtk_widget_show( (GtkWidget*)wSFFD);

        size_t  context_before // agh::alg::ensure_within(
                = (field.selection_start < current_profile->context_pad)
                ? pattern::SPattern<TFloat>::context_pad - field.selection_start
                : pattern::SPattern<TFloat>::context_pad,
                context_after
                = (field.selection_end + pattern::SPattern<TFloat>::context_pad > field.n_samples())
                ? field.n_samples() - field.selection_end
                : pattern::SPattern<TFloat>::context_pad,
                full_sample = run + context_before + context_after;

        // transient is always the last
        if ( profiles.empty() || profiles.back().level != TExpDirLevel::transient )
                profiles.emplace_back(
                        *_p._p.ED,
                        agh::SExpDirLevelId {_p._p.ED->group_of(_p.csubject()), _p.csubject().id, _p.session()});
        {
                auto& P = profiles.back();
                P.thing.resize( full_sample);
                P.thing = field.signal_filtered[ slice (field.selection_start - context_before, full_sample, 1) ];
                P.samplerate = field.samplerate();
                P.context = {context_before, context_after};
                P.Pp = Pp;
                P.criteria = criteria;

                Pp2 = P;
        }

        current_profile = prev(profiles.end());
        populate_combo();

        field_channel_saved = field_channel = &field;

        thing_display_scale = field.signal_display_scale;
        set_thing_da_width( full_sample / field.spp());

        preselect_channel( _p.channel_idx( &field));
        setup_controls_for_find();
        set_profile_manage_buttons_visibility();

        gtk_widget_queue_draw( (GtkWidget*)daSFFDThing);

        return 0;
}
