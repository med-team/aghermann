/*
 *       File name:  aghermann/ui/dirlevel-storable-adapter.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-15
 *
 *         Purpose:  widget class for handling dirlevel storable profiles
 *
 *         License:  GPL
 */

#ifndef AGH_AGHERMANN_UI_DIRLEVEL_STORABLE_ADAPTER_H_
#define AGH_AGHERMANN_UI_DIRLEVEL_STORABLE_ADAPTER_H_

#include "aghermann/expdesign/dirlevel.hh"
#include "aghermann/expdesign/expdesign.hh"
#include "aghermann/ui/misc.hh"
#include "aghermann/ui/mw/mw.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;

namespace agh {
namespace ui {

template <class Storable>
class SDirlevelStorableAdapter {

    public:
        SDirlevelStorableAdapter<Storable> (
                agh::CExpDesign&, const agh::SExpDirLevelId&,
                GtkListStore*&, GtkComboBox*&, gulong&,
                GtkButton*&, GtkButton*&, GtkButton*&,
                GtkDialog*&, GtkEntry*&, GtkToggleButton*&, GtkToggleButton*&, GtkToggleButton*&, GtkButton*&);

        agh::CExpDesign&
               ED;
        agh::SExpDirLevelId
               level_id;

        list<Storable>
                profiles;
        typename list<Storable>::iterator
                current_profile;
        typename list<Storable>::iterator
        profile_by_idx( size_t);

        void load_profiles();
        void save_profiles();
        void discard_current_profile();
        void populate_combo();
        void set_profile_manage_buttons_visibility();

        Storable
                Pp2;

        SUIVarCollection
                W_V;
        void protected_up()
                {
                        suppress_w_v = true;
                        W_V.up();
                        suppress_w_v = false;
                }
        bool    suppress_w_v;

        GtkListStore
                *&mXProfiles;

        GtkComboBox
                *&eXProfileList;
        gulong  &eXProfileList_changed_cb_handler_id;
        GtkButton
                *&bXProfileSave,
                *&bXProfileRevert,
                *&bXProfileDiscard;
        GtkDialog
                *&wXProfileSaveName;
        GtkEntry
                *&eXProfileSaveName;
        GtkToggleButton
                *&eXProfileSaveOriginSubject,
                *&eXProfileSaveOriginExperiment,
                *&eXProfileSaveOriginUser;
        GtkButton
                *&bXProfileSaveOK;

        void eXProfileList_changed_cb();
        void bXProfileSave_clicked_cb();
        void bXProfileDiscard_clicked_cb();
        void bXProfileRevert_clicked_cb();
        void eX_any_profile_value_changed_cb();
        void eXProfileSaveName_changed_cb();
        void eX_any_profile_origin_toggled_cb();

    private:
        void check_profile_name_save_button_label();
};

}}


#include "dirlevel-storable-adapter.ii"

#endif
