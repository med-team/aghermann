/*
 *       File name:  aghermann/globals.cc
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-06-09
 *
 *         Purpose:  global variables, all two of them
 *
 *         License:  GPL
 */

#include "globals.hh"

#include <cstdarg>
#include <cassert>
#include <sys/time.h>
#include <gsl/gsl_rng.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "common/string.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;


gsl_rng *agh::global::rng = nullptr;

int agh::global::num_procs = 1;


void
agh::global::
init( const string& lf_fname)
{
      // 0. log facility
        {
                default_log_facility =
                        lf_fname.empty() ? nullptr : new agh::log::CLogFacility (lf_fname);
        }

      // 1. init rng
        {
                const gsl_rng_type *T;
                gsl_rng_env_setup();
                T = gsl_rng_default;
                if ( gsl_rng_default_seed == 0 ) {
                        struct timeval tp = { 0L, 0L };
                        gettimeofday( &tp, NULL);
                        gsl_rng_default_seed = tp.tv_usec;
                }
                rng = gsl_rng_alloc( T);
        }

#ifdef _OPENMP
      // 2. omp
        {
                num_procs = omp_get_max_threads();
        }
#endif
}


void
agh::global::
fini()
{
        gsl_rng_free( rng);
        rng = nullptr;

        if ( default_log_facility )
                delete default_log_facility;
        default_log_facility = nullptr;
}



agh::log::CLogFacility* agh::global::default_log_facility;

void
agh::global::
log( agh::log::TLevel level, const char* issuer, const char* fmt, ...)
{
        if ( !default_log_facility )
                return;
        va_list ap;
        va_start (ap, fmt);
        default_log_facility -> vmsg( level, issuer, fmt, ap);
        va_end (ap);
}
