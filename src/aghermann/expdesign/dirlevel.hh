/*
 *       File name:  aghermann/expdesign/dirlevel.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2013-09-12
 *
 *         Purpose:  experimental design primary classes: CStorable
 *                   class for various ppacks storable at various dir levels
 *
 *         License:  GPL
 */


#ifndef AGH_AGHERMANN_EXPDESIGN_DIRLEVEL_H_
#define AGH_AGHERMANN_EXPDESIGN_DIRLEVEL_H_


#include <dirent.h>

#include <string>
#include <forward_list>

#include "common/config-validate.hh"
#include "forward-decls.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif


namespace agh {

using namespace std;

enum class TExpDirLevel {
        transient,
        session,
        subject,
        group,
        experiment,
        user,
        system,
};

struct SExpDirLevelId {
        string  g, j, d;
};

const char* exp_dir_level_s( agh::TExpDirLevel);

string level_corrected_subdir( agh::TExpDirLevel, const string&);


class CStorablePPack {
    public:
        CStorablePPack (const string& subdir_, const string& name_, TExpDirLevel level_,
                        CExpDesign& ED_, const SExpDirLevelId& level_id_)
              : subdir (subdir_),
                name (name_),
                level (level_),
                level_id (level_id_),
                ED (ED_),
                saved (false)
                {} // don't load, defer until config_keys_* are connected

        CStorablePPack (const CStorablePPack& rv)   // is ok, just leave alone the config_keys_*
              : subdir (rv.subdir),
                name (rv.name),
                level (rv.level),
                level_id (rv.level_id),
                ED (rv.ED),
                saved (rv.saved)
                {}

        CStorablePPack&
        operator=( const CStorablePPack& rv)
                {
                        subdir = rv.subdir;
                        name = rv.name;
                        level = rv.level;
                        level_id.g = rv.level_id.g;
                        level_id.j = rv.level_id.j;
                        level_id.d = rv.level_id.d;
                        saved = rv.saved;
                        // leave alone config_keys and ED
                        return *this;
                }

        bool
        operator==( const CStorablePPack& rv)
                {
                        return  subdir == rv.subdir &&
                                name == rv.name &&
                                level == rv.level &&
                                level_id.g == rv.level_id.g &&
                                level_id.j == rv.level_id.j &&
                                level_id.d == rv.level_id.d &&
                                saved == rv.saved;
                }

        string  subdir,
                name;
        TExpDirLevel
                level;
        string
        level_corrected_subdir() const
                { return agh::level_corrected_subdir( level, subdir); }

        const char*
        exp_dir_level_s() const
                { return agh::exp_dir_level_s(level); }

        SExpDirLevelId
                level_id;
        CExpDesign&
                ED;

        bool    saved;

        string path() const;

        virtual int load();
        virtual int save();
        virtual int delete_file();
        virtual string serialize() const;
        void touch()
                { saved = false; }

    protected:
        agh::confval::CConfigKeys
                config;
};




int simple_scandir_filter( const struct dirent*);




} // namespace agh

#endif
