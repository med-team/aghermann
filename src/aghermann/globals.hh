/*
 *       File name:  aghermann/globals.hh
 *         Project:  Aghermann
 *          Author:  Andrei Zavada <johnhommer@gmail.com>
 * Initial version:  2010-04-28
 *
 *         Purpose:  global (gasp!) variable definitions
 *
 *         License:  GPL
 */

#ifndef AGH_AGHERMANN_GLOBALS_H_
#define AGH_AGHERMANN_GLOBALS_H_

#include <gsl/gsl_rng.h>
#include "common/log-facility.hh"

#if HAVE_CONFIG_H && !defined(VERSION)
#  include "config.h"
#endif

using namespace std;

namespace agh {
namespace global {

extern gsl_rng *rng;

extern int num_procs;


extern agh::log::CLogFacility* default_log_facility;
void log( agh::log::TLevel, const char* issuer, const char* fmt, ...) __attribute__ ((format (printf, 3, 4)));

#define APPLOG_DEBUG(...) do agh::global::log( agh::log::TLevel::debug,   LOG_SOURCE_ISSUER, __VA_ARGS__); while (0)
#define APPLOG_INFO(...)  do agh::global::log( agh::log::TLevel::info,    LOG_SOURCE_ISSUER, __VA_ARGS__); while (0)
#define APPLOG_WARN(...)  do agh::global::log( agh::log::TLevel::warning, LOG_SOURCE_ISSUER, __VA_ARGS__); while (0)
#define APPLOG_ERROR(...) do agh::global::log( agh::log::TLevel::error,   LOG_SOURCE_ISSUER, __VA_ARGS__); while (0)


void init( const string& lf_fname);
void fini();

} // namespace global
} // namespace agh

#endif
